# indian-express-clone-script
<div dir="ltr" style="text-align: left;" trbidi="on">
<h5 style="background-color: white; box-sizing: border-box; color: #222222; font-family: Poppins; font-size: 14px; font-weight: 400; line-height: 18px; margin-bottom: 10px; margin-top: 0px;">
Quick overview:</h5>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
An online newspaper is the online version of a newspaper, either as a stand-alone publication or as the online version of a printed periodical.News Portal Script is a popular Content Management System developed as a news broadcasting portal.We offer extensive customization (design and development).</div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<br /></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<br />
<section class="box" style="box-sizing: border-box; color: #999999; font-family: Roboto, sans-serif; font-size: 16px;"><div class="row" style="box-sizing: border-box; margin-left: -15px; margin-right: -15px;">
<div class="col-sm-6 wpb_column vc_column_container" style="box-sizing: border-box; float: left; min-height: 1px; padding-left: 15px; padding-right: 15px; position: relative; width: 559px;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<h4 class="vc_custom_heading" style="box-sizing: border-box; color: #26547c; font-size: 18px; font-weight: 400; line-height: 20px; margin: 0px 0px 15px;">
UNIQUE FEATURES:</h4>
<div class="wpb_text_column wpb_content_element " style="box-sizing: border-box;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<ul style="box-sizing: border-box; margin-bottom: 10px; margin-top: 0px;">
<li style="box-sizing: border-box;">Super Admin</li>
<li style="box-sizing: border-box;">Affiliations booking modules</li>
<li style="box-sizing: border-box;">Wallet user modules</li>
<li style="box-sizing: border-box;">Seat seller</li>
<li style="box-sizing: border-box;">White label</li>
<li style="box-sizing: border-box;">Multiple API integrations</li>
<li style="box-sizing: border-box;">Own bus inventory management</li>
<li style="box-sizing: border-box;">Unique Mark up, Commission and service charge for agent wise</li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-sm-6 wpb_column vc_column_container" style="box-sizing: border-box; float: left; min-height: 1px; padding-left: 15px; padding-right: 15px; position: relative; width: 559px;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<h4 class="vc_custom_heading" style="box-sizing: border-box; color: #26547c; font-size: 18px; font-weight: 400; line-height: 20px; margin: 0px 0px 15px;">
GENERAL FEATURES:</h4>
<div class="wpb_text_column wpb_content_element " style="box-sizing: border-box;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<ul style="box-sizing: border-box; margin-bottom: 10px; margin-top: 0px;">
<li style="box-sizing: border-box;">Highly Customizable and Flexible</li>
<li style="box-sizing: border-box;">Wallet payment</li>
<li style="box-sizing: border-box;">Guest user access</li>
<li style="box-sizing: border-box;">User-friendly Interface</li>
<li style="box-sizing: border-box;">Eye-catching ui design and color combination</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</section><section class="box" style="box-sizing: border-box; color: #999999; font-family: Roboto, sans-serif; font-size: 16px;"><div class="row" style="box-sizing: border-box; margin-left: -15px; margin-right: -15px;">
<div class="col-sm-6 wpb_column vc_column_container" style="box-sizing: border-box; float: left; min-height: 1px; padding-left: 15px; padding-right: 15px; position: relative; width: 559px;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<h4 class="vc_custom_heading" style="box-sizing: border-box; color: #26547c; font-size: 18px; font-weight: 400; line-height: 20px; margin: 0px 0px 15px;">
USER PANEL:</h4>
<div class="wpb_text_column wpb_content_element " style="box-sizing: border-box;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<ul style="box-sizing: border-box; margin-bottom: 10px; margin-top: 0px;">
<li style="box-sizing: border-box;">Search option is customizable to include the City, Check-in Date and Check-out Date</li>
<li style="box-sizing: border-box;">Searching hotel categories option is available</li>
<li style="box-sizing: border-box;">Recently added hotels and their listings can be easily viewed</li>
<li style="box-sizing: border-box;">Beautiful home page banner highlighting the hotels</li>
<li style="box-sizing: border-box;">The customer can check room availability and book based on room type</li>
<li style="box-sizing: border-box;">The customer can leave a review and rating of each hotel</li>
<li style="box-sizing: border-box;">Accounts dashboard and option to manage sensitive data like username and password with ease</li>
<li style="box-sizing: border-box;">Highlighted “Special Prices” for hotels</li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-sm-6 wpb_column vc_column_container" style="box-sizing: border-box; float: left; min-height: 1px; padding-left: 15px; padding-right: 15px; position: relative; width: 559px;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<h4 class="vc_custom_heading" style="box-sizing: border-box; color: #26547c; font-size: 18px; font-weight: 400; line-height: 20px; margin: 0px 0px 15px;">
ADMIN PANEL:</h4>
<div class="wpb_text_column wpb_content_element " style="box-sizing: border-box;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<ul style="box-sizing: border-box; margin-bottom: 10px; margin-top: 0px;">
<li style="box-sizing: border-box;">Can add more as many hotels as you want with a unique order ID for every booking</li>
<li style="box-sizing: border-box;">With each entry, you can enter the Hotel Name, Its Short Description, and its SKU and URL Key</li>
<li style="box-sizing: border-box;">SEO-Friendly features including option to include Meta Tags</li>
<li style="box-sizing: border-box;">Option to display all important booking details</li>
<li style="box-sizing: border-box;">Easy managing of all hotel orders and bookings</li>
<li style="box-sizing: border-box;">Can manually approve all customer reviews before they are published</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</section><section class="box" style="box-sizing: border-box; color: #999999; font-family: Roboto, sans-serif; font-size: 16px;"><div class="row" style="box-sizing: border-box; margin-left: -15px; margin-right: -15px;">
<div class="col-sm-6 wpb_column vc_column_container" style="box-sizing: border-box; float: left; min-height: 1px; padding-left: 15px; padding-right: 15px; position: relative; width: 559px;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<h4 class="vc_custom_heading" style="box-sizing: border-box; color: #26547c; font-size: 18px; font-weight: 400; line-height: 20px; margin: 0px 0px 15px;">
AGENT PANEL:</h4>
<div class="wpb_text_column wpb_content_element " style="box-sizing: border-box;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<div style="box-sizing: border-box;">
Login<br />
Control panel<br />
Booking report<br />
Reports<br />
Wallet deposit<br />
Print bookings<br />
Cancellation</div>
</div>
</div>
</div>
</div>
<div class="col-sm-6 wpb_column vc_column_container" style="box-sizing: border-box; float: left; min-height: 1px; padding-left: 15px; padding-right: 15px; position: relative; width: 559px;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<h4 class="vc_custom_heading" style="box-sizing: border-box; color: #26547c; font-size: 18px; font-weight: 400; line-height: 20px; margin: 0px 0px 15px;">
WALLET USER PANEL:</h4>
<div class="wpb_text_column wpb_content_element " style="box-sizing: border-box;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<div style="box-sizing: border-box;">
Sign in sign up options<br />
Personal&nbsp;<a href="https://www.doditsolutions.com/my-account/" style="background-color: transparent; box-sizing: border-box; color: #ef476f; cursor: pointer; outline: 0px; text-decoration-line: none; transition: all 0.4s ease;">my account</a>&nbsp;details with history<br />
Reports for all booked and cancelled rooms<br />
Get Email Options<br />
Get SMS Options<br />
Search option enabled for city<br />
Check on available Hotel details and rooms<br />
Can view en-numbers of hotel snaps and videos<br />
User Wallet available<br />
Payment Gateway enabled<br />
Print bookings<br />
Cancel bookings<br />
Fund transfer<br />
Check refund status by entering the booking number<br />
Coupon codes<br />
Promotions codes<br />
Wallet offers credits<br />
Refer a friends…etc</div>
</div>
</div>
</div>
</div>
</div>
</section><section class="box" style="box-sizing: border-box; color: #999999; font-family: Roboto, sans-serif; font-size: 16px;"><div class="row" style="box-sizing: border-box; margin-left: -15px; margin-right: -15px;">
<div class="col-sm-6 wpb_column vc_column_container" style="box-sizing: border-box; float: left; min-height: 1px; padding-left: 15px; padding-right: 15px; position: relative; width: 559px;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<h4 class="vc_custom_heading" style="box-sizing: border-box; color: #26547c; font-size: 18px; font-weight: 400; line-height: 20px; margin: 0px 0px 15px;">
GUEST USER PANEL:</h4>
<div class="wpb_text_column wpb_content_element " style="box-sizing: border-box;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<div style="box-sizing: border-box;">
Search option enabled for city<br />
Check on available Hotel details and rooms<br />
Can view en-numbers of hotels snaps and videos<br />
Fill details and book it<br />
Payment Gateway enabled<br />
Print bookings<br />
Cancel bookings<br />
Check refund status by entering the ticket number<br />
Coupon codes<br />
Promotions codes<br />
Wallet offers credits<br />
Refer a friends…etc</div>
<div style="box-sizing: border-box;">
<br /></div>
<div style="box-sizing: border-box;">
Check Out Our Product in:</div>
<div style="box-sizing: border-box;">
<br /></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="background-color: transparent; color: black; font-family: &quot;arial&quot;; font-size: 11pt; font-style: normal; font-variant: normal; font-weight: 400; text-decoration: none; vertical-align: baseline; white-space: pre;"><a href="https://www.doditsolutions.com/">https://www.doditsolutions.com/</a></span></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="background-color: transparent; color: black; font-family: &quot;arial&quot;; font-size: 11pt; font-style: normal; font-variant: normal; font-weight: 400; text-decoration: none; vertical-align: baseline; white-space: pre;"><a href="http://scriptstore.in/">http://scriptstore.in/</a></span></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="background-color: transparent; color: black; font-family: &quot;arial&quot;; font-size: 11pt; font-style: normal; font-variant: normal; font-weight: 400; text-decoration: none; vertical-align: baseline; white-space: pre;"><a href="http://phpreadymadescripts.com/">http://phpreadymadescripts.com/</a></span></div>
<div style="box-sizing: border-box;">
<span id="docs-internal-guid-2728dadd-c6a6-042d-21a4-75ec93a90151"><br /></span></div>
</div>
</div>
</div>
</div>
</div>
</section></div>
</div>
